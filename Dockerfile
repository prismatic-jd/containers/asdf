# Use a lightweight base image with only the necessary packages installed
FROM registry.gitlab.com/prismatic-jd/containers/base:0.1.0

# Set labels to provide metadata
LABEL maintainer="Prismatic Dev <dev@prismatic.io>"
LABEL updated="2023-03-20"

# Set environment variables
ENV LATEST=0.11.2
ENV PATH="${PATH}:${SERVICE_USER_HOME}/.asdf/shims:${SERVICE_USER_HOME}/.asdf/bin"

# Switch to non-root user
# USER ${SERVICE_USER}

# Set working directory
WORKDIR ${SERVICE_USER_HOME}

# Set argument for the CI_COMMIT_REF_NAME
ARG CI_COMMIT_REF_NAME
ARG CI_PROJECT_DIR

# Clone asdf repository from GitHub
RUN git config --global --add safe.directory "${CI_PROJECT_DIR}" \
    && git clone --depth 1 \
    https://github.com/asdf-vm/asdf.git "${SERVICE_USER_HOME}/.asdf" \
    --branch "v$(test "${CI_COMMIT_REF_NAME}" = "main" && echo "${LATEST}" || echo "${CI_COMMIT_REF_NAME}")"

# Append a line to both ~/.bashrc and ~/.profile files to source the "asdf" tool's shell script
RUN printf "\n. %s/.asdf/asdf.sh" "${SERVICE_USER_HOME}" >> "${SERVICE_USER_HOME}/.bashrc" && \
    printf "\n. %s/.asdf/asdf.sh" "${SERVICE_USER_HOME}" >> "${SERVICE_USER_HOME}/.profile" && \
    chown "${SERVICE_USER}:${SERVICE_USER}" "${SERVICE_USER_HOME}/.bashrc" && \
    chown "${SERVICE_USER}:${SERVICE_USER}" "${SERVICE_USER_HOME}/.profile"

# Source the .bashrc file to ensure that the changes made to the file take effect in the current shell session
RUN /bin/bash -c "source ${SERVICE_USER_HOME}/.bashrc"
