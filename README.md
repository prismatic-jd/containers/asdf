# Dockerfile for Node.js with asdf

This Dockerfile creates a Docker image with the `asdf` version manager installed, based on a lightweight base image. The `asdf` tool allows you to manage multiple language runtimes and tools. The image installs only the necessary packages and sets environment variables for `SERVICE_HOME` and `SERVICE_USER` paths. It also clones the `asdf` repository from GitHub, checks out the appropriate branch based on the `CI_COMMIT_REF_NAME` argument, and appends a line to the `.bashrc` and `.profile` files to source the `asdf` tool's shell script. Finally, the Dockerfile switches to a non-root user, sets the working directory, and sources the `.bashrc` file to ensure the changes take effect.

## Building the Docker Image

To build the Docker image, navigate to the directory containing the Dockerfile and run the following command:

```bash
docker build -t asdf .
```

## Running a Container from the Image
To run a container from the built image, use the following command:
```bash
docker run -it --rm your-image-name
```

### Using in Child Containers
To use this image as a base for other containers, add the following line at the beginning of the child container's Dockerfile:
```
FROM asdf
```


## Using hadolint with This Dockerfile

To ensure the Dockerfile follows best practices and guidelines,we use hadolint to lint the Dockerfile in this project. Hadolint is a popular Dockerfile linter that checks for potential issues and violations of Docker best practices.

### Installing hadolint
### macOS
1. Install Homebrew if you haven't already:
  ```bash
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  ```
2.  Install hadolint using Homebrew:
#### Arch Linux
For Arch Linux, you can install hadolint from the [Arch User Repository (AUR)](https://aur.archlinux.org/packages/hadolint-bin/). Use your preferred AUR helper or follow these steps with `yay`:

1. Install hadolint using `yay`:
  ```bash
  yay -S hadolint-bin
  ```



### Running hadolint

After installing hadolint, you can use it to lint the Dockerfile of this project. In the directory containing the Dockerfile, run:

```bash
hadolint Dockerfile
```
Hadolint will analyze the Dockerfile and report any issues it finds. If it detects any problems or violations of best practices, it will display a list of warnings and errors with explanations and possible solutions.

You can then update the Dockerfile to address the identified issues and rerun hadolint to ensure the Dockerfile is in good shape.
